﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class AcceptHttpResponseHandler : IHttpResponseHandler
    {
        private readonly IEnumerable<int> acceptedHttpStatusCodes;

        public AcceptHttpResponseHandler(params int[] acceptedHttpStatusCodes)
        {
            this.acceptedHttpStatusCodes = acceptedHttpStatusCodes;
        }

        #region implementing IHttpResponseHandler

        bool IHttpResponseHandler.CanHandle(int httpStatusCode)
        {
            return IsHttpStatusCodeAccepted(httpStatusCode);
        }

        void IHttpResponseHandler.HandleResponse(HttpClient request, HttpResponseMessage response)
        {
            var statusCode = response.GetStatusCode();

            if (IsHttpStatusCodeAccepted(statusCode))
                return;

            throw new HttpResponseUnhandledException(statusCode);
        }

        #endregion

        private bool IsHttpStatusCodeAccepted(int httpStatusCode)
        {
            return acceptedHttpStatusCodes.Contains(httpStatusCode);
        }
    }
}
