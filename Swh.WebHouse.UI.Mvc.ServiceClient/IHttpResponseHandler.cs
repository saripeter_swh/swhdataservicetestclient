﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public interface IHttpResponseHandler
    {
        bool CanHandle(int httpStatusCode);
        void HandleResponse(HttpClient request, HttpResponseMessage response);
    }
}
