﻿using System;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    [Serializable]
    public class HttpErrorOccuredException : Exception
    {
        public int HttpStatusCode { get; }
        public HttpError HttpError { get; }

        public HttpErrorOccuredException(int httpStatusCode, HttpError httpError)
            : base(httpError.ExceptionMessage)
        {
            HttpStatusCode = httpStatusCode;
            HttpError = httpError;
        }
    }
}
