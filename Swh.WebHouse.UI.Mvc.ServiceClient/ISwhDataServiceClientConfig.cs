﻿using System;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public interface ISwhDataServiceClientConfig
    {
        string ApplicationId { get; }
        string AuthToken { get; }
        string Culture { get; }
        bool IsAuthCall { get; }
        Guid TranId { get; set; }
        string AuthMode { get; }
        int OffsetMinutes { get; }
    }
}
