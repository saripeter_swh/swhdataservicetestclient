﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class HttpErrorHandler : IHttpResponseHandler
    {
        #region implementing IHttpResponseHandler

        bool IHttpResponseHandler.CanHandle(int httpStatusCode)
        {
            return true;
        }

        void IHttpResponseHandler.HandleResponse(HttpClient request, HttpResponseMessage response)
        {
            var statusCode = response.GetStatusCode();

            var content = response.Content.ReadAsStringAsync().Result;

            var httpError = JsonConvert.DeserializeObject<HttpError>(response.Content.ReadAsStringAsync().Result);

            throw new HttpErrorOccuredException(statusCode, httpError);
        }

        #endregion

        #region singleton instance

        private static HttpErrorHandler instance;

        private HttpErrorHandler()
        {
        }

        public static HttpErrorHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HttpErrorHandler();
                }
                return instance;
            }
        }

        #endregion
    }
}
