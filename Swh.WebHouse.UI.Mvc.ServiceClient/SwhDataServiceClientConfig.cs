﻿using System;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class SwhDataServiceClientConfig : ISwhDataServiceClientConfig
    {
        public string ApplicationId { get; }
        public string AuthToken { get; }
        public string Culture { get; }
        public bool IsAuthCall { get; }
        public Guid TranId { get; set; }
        public string AuthMode { get; }
        public int OffsetMinutes { get; }

        public SwhDataServiceClientConfig(string applicationId, string authToken, string culture, bool isAuthCall)
        {
            ApplicationId = applicationId;
            AuthToken = authToken;
            Culture = culture;
            IsAuthCall = isAuthCall;
            TranId=Guid.NewGuid();
            OffsetMinutes = 60;

            //AuthMode = isAuthCall ? "TRUE" : "SWH";
            AuthMode = isAuthCall ? "TRUE" : "";
        }
    }
}