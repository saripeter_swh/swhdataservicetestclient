﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    [Serializable]
    public class ValidationException : Exception
    {
        public IEnumerable<IValidationResult> Results { get; }

        public ValidationException(params IValidationResult[] results)
            : base($"An error occured during validating the request! See '{nameof(Results)}' for details")
        {
            Results = results;
        }
    }
}
