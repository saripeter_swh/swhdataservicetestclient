﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class CompositeHttpResponseHandler : IHttpResponseHandler
    {
        private readonly IEnumerable<IHttpResponseHandler> httpResponsehandlers;

        public CompositeHttpResponseHandler(params IHttpResponseHandler[] httpResponsehandlers)
        {
            this.httpResponsehandlers = httpResponsehandlers;
        }

        #region implementing IHttpResponseHandler

        bool IHttpResponseHandler.CanHandle(int httpStatusCode)
        {
            return IsHttpStatusCodeAccepted(httpStatusCode);
        }

        void IHttpResponseHandler.HandleResponse(HttpClient request, HttpResponseMessage response)
        {
            var statusCode = response.GetStatusCode();

            var handler = GetHandlerFor(statusCode);

            handler.HandleResponse(request, response);
        }

        #endregion

        private bool IsHttpStatusCodeAccepted(int httpStatusCode)
        {
            return httpResponsehandlers.Any(handler => handler.CanHandle(httpStatusCode));
        }

        private IHttpResponseHandler GetHandlerFor(int statusCode)
        {
            var firstHandler = httpResponsehandlers.First(handler => handler.CanHandle(statusCode));

            if (firstHandler == null)
                throw new HttpResponseUnhandledException(statusCode);

            return firstHandler;
        }
    }
}
