﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    internal class ValidationResult : IValidationResult
    {
        public string ErrorMessage { get; }

        public IEnumerable<string> MemberNames { get; }

        public ValidationResult(string errorMessage, IEnumerable<string> memberNames)
        {
            ErrorMessage = errorMessage;
            MemberNames = memberNames;
        }
    }
}
