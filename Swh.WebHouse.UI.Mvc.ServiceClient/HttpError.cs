﻿using System;
using Newtonsoft.Json;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class HttpError
    {
        public string ExceptionMessage { get; }

        public string ExceptionType { get; }

        public string Message { get; }

        public string MessageDetail { get; }

        public string StackTrace { get; }

        public HttpError InnerException { get; set; }

        public HttpError(
            string exceptionMessage,
            string exceptionType, 
            string message,
            string messageDetail,
            string stackTrace,
            HttpError innerException)
        {
            ExceptionMessage = exceptionMessage;
            ExceptionType = exceptionType;
            Message = message;
            MessageDetail = messageDetail;
            StackTrace = stackTrace;
            InnerException = innerException;
        }
    }
}
