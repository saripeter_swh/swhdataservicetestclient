﻿
using Swh.DataService.Contract.Interfaces.Classes;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    //public class DeleteAddressCommand
    //{
    //    public Guid AddressId { get; set; }
    //}

    //public class CreateAddressCommand
    //{
    //    public Address Address { get; set; }
    //}

    //public class UpdateAddressCommand
    //{
    //    public Address Address { get; set; }
    //}

    public static class Storage
    {
        //public static AddressMainListResult[] db = new AddressMainListResult[]
        //    {
        //                new AddressMainListResult { Address = new Address { Id = new Guid("3dc3476c-b6fd-4b9a-8d1d-985801bc99ee"), PartnerID = new Guid("1c370c96-d0ec-4a1b-9ab7-04eb4d5fa377"), AddressTypeID = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9"), Comment = "A" } },
        //                new AddressMainListResult { Address = new Address { Id = new Guid("afc461c9-6b3a-4720-8265-1d4f7403dcff"), PartnerID = new Guid("1c370c96-d0ec-4a1b-9ab7-04eb4d5fa377"), AddressTypeID = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9"), Comment = "B" } },
        //                new AddressMainListResult { Address = new Address { Id = new Guid("0c6aaa63-e11e-4e0b-8bc9-4b6b1389cdfb"), PartnerID = new Guid("1c370c96-d0ec-4a1b-9ab7-04eb4d5fa377"), AddressTypeID = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38"), Comment = "C" } },
        //                new AddressMainListResult { Address = new Address { Id = new Guid("c26b3a92-7e1e-42b3-9d41-5a55ff1d4918"), PartnerID = new Guid("f9aa7b47-7057-44a4-9ac8-4794f2269263"), AddressTypeID = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38"), Comment = "D" } },
        //                new AddressMainListResult { Address = new Address { Id = new Guid("46f37176-db96-46a2-a791-94bd3182d228"), PartnerID = new Guid("f9aa7b47-7057-44a4-9ac8-4794f2269263"), AddressTypeID = new Guid("9ca82100-7898-409c-8fde-c7604eedbe58"), Comment = "E" } },
        //    };
    }

    public partial class QueriesClient
    {
        public Task<Paged<PartnerTypeMainListResult>> PartnerTypeMainListAsync(int? paging_pageIndex, int? paging_pageSize)
        {
            return PartnerTypeMainListAsync(paging_pageIndex, paging_pageSize, CancellationToken.None);
        }

        public async Task<Paged<PartnerTypeMainListResult>> PartnerTypeMainListAsync(int? paging_pageIndex, int? paging_pageSize, CancellationToken cancellationToken)
        {
            var result = new Paged<PartnerTypeMainListResult>();
            result.CollectionItemCount = 2;
            result.Items = new PartnerTypeMainListResult[]
            {
                new PartnerTypeMainListResult { Partner = new PartnerType { Name = "PartnerTest1", Id = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9") } },
                new PartnerTypeMainListResult { Partner = new PartnerType { Name = "PartnerTest2", Id = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38") } },
                new PartnerTypeMainListResult { Partner = new PartnerType { Name = "PartnerTest3", Id = new Guid("9ca82100-7898-409c-8fde-c7604eedbe58") } },
            };
            result.Paging = new PageInfo();
            result.Paging.PageIndex = paging_pageIndex.Value;
            result.Paging.PageSize = paging_pageSize.Value;
            return result;
        }

        public Task<Paged<PartnerCityMainListResult>> PartnerCityMainListAsync(Guid? optional_partnerTypeId, int? paging_pageIndex, int? paging_pageSize)
        {
            return PartnerCityMainListAsync(optional_partnerTypeId, paging_pageIndex, paging_pageSize, CancellationToken.None);
        }

        public async Task<Paged<PartnerCityMainListResult>> PartnerCityMainListAsync(Guid? optional_partnerTypeId, int? paging_pageIndex, int? paging_pageSize, CancellationToken cancellationToken)
        {
            var result = new Paged<PartnerCityMainListResult>();
            result.Items = new PartnerCityMainListResult[]
            {
                new PartnerCityMainListResult { City = new PartnerCity { Name = "CityTest1", Id = new Guid("3dc3476c-b6fd-4b9a-8d1d-985801bc99ee"), PartnerTypeId = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9") } },
                new PartnerCityMainListResult { City = new PartnerCity { Name = "CityTest2", Id = new Guid("afc461c9-6b3a-4720-8265-1d4f7403dcff"), PartnerTypeId = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38") } },
                new PartnerCityMainListResult { City = new PartnerCity { Name = "CityTest3", Id = new Guid("0c6aaa63-e11e-4e0b-8bc9-4b6b1389cdfb"), PartnerTypeId = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38") } },
                new PartnerCityMainListResult { City = new PartnerCity { Name = "CityTest4", Id = new Guid("c26b3a92-7e1e-42b3-9d41-5a55ff1d4918"), PartnerTypeId = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9") } },
                new PartnerCityMainListResult { City = new PartnerCity { Name = "CityTest5", Id = new Guid("46f37176-db96-46a2-a791-94bd3182d228"), PartnerTypeId = new Guid("9ca82100-7898-409c-8fde-c7604eedbe58") } },
            };
            if (optional_partnerTypeId != null)
            {
                result.Items = result.Items.Where(item => item.City.PartnerTypeId == optional_partnerTypeId).ToArray();
            }
            result.CollectionItemCount = result.Items.Length;
            result.Paging = new PageInfo();
            result.Paging.PageIndex = paging_pageIndex.Value;
            result.Paging.PageSize = paging_pageSize.Value;
            return result;
        }

        //public Task<Paged<AddressMainListResult>> AddressMainListAsync(Guid? optional_partnerId, int? paging_pageIndex, int? paging_pageSize)
        //{
        //    return AddressMainListAsync(optional_partnerId, paging_pageIndex, paging_pageSize, CancellationToken.None);
        //}

        //public async Task<Paged<AddressMainListResult>> AddressMainListAsync(Guid? optional_partnerId, int? paging_pageIndex, int? paging_pageSize, CancellationToken cancellationToken)
        //{
        //    var result = new Paged<AddressMainListResult>();
        //    result.Items = Storage.db;
        //    if (optional_partnerId != null)
        //    {
        //        result.Items = result.Items.Where(item => item.Address.PartnerID == optional_partnerId).ToArray();
        //    }
        //    result.CollectionItemCount = result.Items.Length;
        //    result.Paging = new PageInfo();
        //    result.Paging.PageIndex = paging_pageIndex.Value;
        //    result.Paging.PageSize = paging_pageSize.Value;
        //    return result;
        //}

        //public async Task<Address> AddressReadAsync(Guid addressId, CancellationToken cancellationToken)
        //{
        //    var result = new Paged<AddressMainListResult>();
        //    result.Items = Storage.db;
        //    if (addressId != null)
        //    {
        //        return result.Items.Where(item => item.Address.Id == addressId).Select(item => item.Address).SingleOrDefault();
        //    }
        //    return null;
        //}

        public Task<Paged<AddressTypeMainListResult>> AddressTypeMainListAsync(int? paging_pageIndex, int? paging_pageSize)
        {
            return AddressTypeMainListAsync(paging_pageIndex, paging_pageSize, CancellationToken.None);
        }

        public async Task<Paged<AddressTypeMainListResult>> AddressTypeMainListAsync(int? paging_pageIndex, int? paging_pageSize, CancellationToken cancellationToken)
        {
            var result = new Paged<AddressTypeMainListResult>();
            result.CollectionItemCount = 2;
            result.Items = new AddressTypeMainListResult[]
            {
                new AddressTypeMainListResult { Address = new AddressType { Name = "AddressTest1", Id = new Guid("f4e85d28-b736-494a-8808-a9eed7133bc9") } },
                new AddressTypeMainListResult { Address = new AddressType { Name = "AddressTest2", Id = new Guid("eb1e5a4e-b342-4778-8d4a-411b08b13d38") } },
                new AddressTypeMainListResult { Address = new AddressType { Name = "AddressTest3", Id = new Guid("9ca82100-7898-409c-8fde-c7604eedbe58") } },
            };
            result.Paging = new PageInfo();
            result.Paging.PageIndex = paging_pageIndex.Value;
            result.Paging.PageSize = paging_pageSize.Value;
            return result;
        }
    }

    //public partial class CommandsClient
    //{

    //    public Task DeleteAddressAsync(DeleteAddressCommand command)
    //    {
    //        return DeleteAddressAsync(command, CancellationToken.None);
    //    }

    //    public async Task DeleteAddressAsync(DeleteAddressCommand command, CancellationToken cancellationToken)
    //    {
    //        Storage.db = Storage.db.Where(item => item.Address.Id != command.AddressId).ToArray();
    //        return;
    //    }

    //    public Task CreateAddressAsync(CreateAddressCommand command)
    //    {
    //        return CreateAddressAsync(command, CancellationToken.None);
    //    }

    //    public async Task CreateAddressAsync(CreateAddressCommand command, CancellationToken cancellationToken)
    //    {
    //        var list = Storage.db.ToList();
    //        list.Add(new AddressMainListResult { Address = command.Address });
    //        Storage.db = list.ToArray();
    //        return;
    //    }

    //    public Task UpdateAddressAsync(UpdateAddressCommand command)
    //    {
    //        return UpdateAddressAsync(command, CancellationToken.None);
    //    }

    //    public async Task UpdateAddressAsync(UpdateAddressCommand command, CancellationToken cancellationToken)
    //    {
    //        var i = 0;
    //        var ok = false;
    //        for(; ok = i<Storage.db.Length; i++)
    //        {
    //            if (command.Address.IdP == Storage.db[i].Address.Id)
    //                break;
    //        }
    //        if(ok)
    //        {
    //            Storage.db[i].Address = command.Address;
    //        }
    //        return;
    //    }
    //}
}
