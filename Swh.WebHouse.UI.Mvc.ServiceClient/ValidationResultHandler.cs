﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class ValidationResultHandler : IHttpResponseHandler
    {
        private readonly IEnumerable<int> acceptedHttpStatusCodes;

        public ValidationResultHandler(params int[] acceptedHttpStatusCodes)
        {
            this.acceptedHttpStatusCodes = acceptedHttpStatusCodes;
        }

        #region implementing IHttpResponseHandler

        bool IHttpResponseHandler.CanHandle(int httpStatusCode)
        {
            return IsHttpStatusCodeAccepted(httpStatusCode);
        }

        void IHttpResponseHandler.HandleResponse(HttpClient request, HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            var validationResult = new ValidationResult[1];

            bool isDeserialized =
                TryDeserializeContentAs<ValidationResult>(content, out validationResult[0]) ||
                TryDeserializeContentAs<ValidationResult[]>(content, out validationResult);
            if (isDeserialized)
            {
                throw new ValidationException(validationResult);
            }
            else
            {
                throw new InvalidValidationResultException(content);
            }
        }

        #endregion

        private bool IsHttpStatusCodeAccepted(int httpStatusCode)
        {
            return acceptedHttpStatusCodes.Contains(httpStatusCode);
        }

        private bool TryDeserializeContentAs<T>(string content, out T result, T @default = default(T))
        {
            try
            {
                result = JsonConvert.DeserializeObject<T>(content);

                return true;
            }
            catch (JsonSerializationException)
            {
                result = @default;

                return false;
            }
        }
    }
}
