﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    [Obsolete("Use ValidationResultHandler instead.")]
    public class CompositeValidationResultHandler : IHttpResponseHandler
    {
        private readonly IEnumerable<int> acceptedHttpStatusCodes;

        public CompositeValidationResultHandler(params int[] acceptedHttpStatusCodes)
        {
            this.acceptedHttpStatusCodes = acceptedHttpStatusCodes;
        }

        #region implementing IHttpResponseHandler

        bool IHttpResponseHandler.CanHandle(int httpStatusCode)
        {
            return IsHttpStatusCodeAccepted(httpStatusCode);
        }

        void IHttpResponseHandler.HandleResponse(HttpClient request, HttpResponseMessage response)
        {
            var content = response.Content.ReadAsStringAsync().Result;

            var validationResult = JsonConvert.DeserializeObject<ValidationResult[]>(response.Content.ReadAsStringAsync().Result);

            throw new ValidationException(validationResult);
        }

        #endregion

        private bool IsHttpStatusCodeAccepted(int httpStatusCode)
        {
            return acceptedHttpStatusCodes.Contains(httpStatusCode);
        }
    }
}
