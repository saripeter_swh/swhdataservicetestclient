﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public interface IValidationResult
    {
        string ErrorMessage { get; }

        IEnumerable<string> MemberNames { get; }
    }
}
