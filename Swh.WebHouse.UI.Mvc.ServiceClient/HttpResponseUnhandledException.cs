﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    [Serializable]
    public class HttpResponseUnhandledException : Exception
    {
        public int HttpStatusCode { get; }

        internal HttpResponseUnhandledException(int httpStatusCode)
        {
            HttpStatusCode = httpStatusCode;
        }
    }
}
