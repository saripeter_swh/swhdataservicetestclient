﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public static class ServiceClientHelper
    {
        private static IHttpResponseHandler query;

        private static IHttpResponseHandler command;

        private static IHttpResponseHandler CompositeResponseHandler(int acceptStatusCode)
        {
            var acceptHttpResponseHandler = new AcceptHttpResponseHandler(acceptStatusCode);

            var validationResultHandler = new ValidationResultHandler(HttpStatusCode.BadRequest);

            return new CompositeHttpResponseHandler(acceptHttpResponseHandler, validationResultHandler, HttpErrorHandler.Instance);
        }


        public static IHttpResponseHandler Query
        {
            get
            {
                if(query == null)
                    query = CompositeResponseHandler(HttpStatusCode.OK);
                return query;
            }
        }

        public static IHttpResponseHandler Command
        {
            get
            {
                if (command == null)
                    command = CompositeResponseHandler(HttpStatusCode.NoContent);
                return command;
            }
        }
    }

    public partial class QueriesClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static QueriesClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class CommandsClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CommandsClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class CreateUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreateUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class UpdateUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UpdateUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class DeleteUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class UserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class DeleteMultipleLicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteMultipleLicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class DeleteAllLicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteAllLicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }

    public partial class CreateLicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreateLicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeleteLicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteLicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class UpdateLicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UpdateLicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class CreatePartnerClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreatePartnerClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeletePartnerClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeletePartnerClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class UpdatePartnerClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UpdatePartnerClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class LicenseClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static LicenseClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class PartnerClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static PartnerClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeleteAllUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteAllUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class CreateManyUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreateManyUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeleteManyUserClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteManyUserClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class PublicCommandsClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static PublicCommandsClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class AppTenantClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static AppTenantClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class CreateAppTenantClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreateAppTenantClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeleteAppTenantClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteAppTenantClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class UpdateAppTenantClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UpdateAppTenantClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class CreateTransportCargoClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static CreateTransportCargoClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class DeleteTransportCargoClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static DeleteTransportCargoClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler
        {
            get
            {
                return defaultHttpResponseHandler;
            }
        }

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class TransportCargoClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static TransportCargoClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class UpdateTransportCargoClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static UpdateTransportCargoClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Command;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
    public partial class PublicQueriesClient
    {
        private static readonly IHttpResponseHandler defaultHttpResponseHandler;

        static PublicQueriesClient()
        {
            defaultHttpResponseHandler = ServiceClientHelper.Query;
        }

        protected override IHttpResponseHandler DefaultHttpResponseHandler => defaultHttpResponseHandler;

        partial void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            base.ProcessResponse(request, response);
        }
    }
}
