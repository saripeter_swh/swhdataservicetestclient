﻿using System;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    /// <summary>
    /// Contains values of http status codes.
    /// </summary>
    internal static class HttpStatusCode
    {
        public const int OK = 200;
        public const int NoContent = 204;
        public const int BadRequest = 400;
        public const int Unauthorized = 401;
    }
}
