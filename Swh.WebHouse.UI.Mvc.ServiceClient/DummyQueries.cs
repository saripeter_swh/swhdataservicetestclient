﻿using Swh.DataService.Contract.DTOs.Master.Partner.Entities;
using Swh.DataService.Contract.Interfaces;
using Swh.DataService.Contract.Interfaces.Classes;
using System;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public class PartnerType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class PartnerTypeMainListResult
    {
        public PartnerType Partner { get; set; }
    }

    public class PartnerTypeMainListQuery : IQuery<Paged<PartnerTypeMainListResult>>
    {
        public PartnerTypeMainListQuery()
        {
            Paging = new PageInfo { PageIndex = 0, PageSize = 200 };
        }
        public PageInfo Paging { get; set; }
    }

    public class PartnerCity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid PartnerTypeId { get; set; }
    }

    public class PartnerCityMainListResult
    {
        public PartnerCity City { get; set; }
    }

    public class PartnerCityMainListQuery : IQuery<Paged<PartnerCityMainListResult>>
    {
        public PartnerCityMainListQuery()
        {
            Paging = new PageInfo { PageIndex = 0, PageSize = 200 };
            Optional = new PartnerCityMainListQueryOptional();
        }
        public PageInfo Paging { get; set; }
        public PartnerCityMainListQueryOptional Optional { get; set; }

        public class PartnerCityMainListQueryOptional
        {
            public PartnerCityMainListQueryOptional()
            {

            }

            public Guid? PartnerTypeID { get; set; }
        }
    }

    //public class AddressMainListResult
    //{
    //    public Address Address { get; set; }
    //}

    //public class AddressMainListQuery : IQuery<Paged<AddressMainListResult>>
    //{
    //    public AddressMainListQuery()
    //    {
    //        Paging = new PageInfo { PageIndex = 0, PageSize = 200 };
    //    }
    //    public PageInfo Paging { get; set; }
    //    public AddressMainListQueryOptional Optional { get; set; }

    //    public class AddressMainListQueryOptional
    //    {
    //        public AddressMainListQueryOptional()
    //        {

    //        }

    //        public Guid? PartnerID { get; set; }
    //    }
    //}

    public class AddressType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class AddressTypeMainListResult
    {
        public AddressType Address { get; set; }
    }

    public class AddressTypeMainListQuery : IQuery<Paged<AddressTypeMainListResult>>
    {
        public AddressTypeMainListQuery()
        {
            Paging = new PageInfo { PageIndex = 0, PageSize = 200 };
        }
        public PageInfo Paging { get; set; }
    }
}