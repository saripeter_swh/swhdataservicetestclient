﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    [Serializable]
    public class InvalidValidationResultException : Exception
    {
        public string Content { get; }

        public InvalidValidationResultException(string content)
            : base($"Cannot deserialize a valid object from content. See {nameof(Content)} for details.")
        {
            Content = content;
        }
    }
}
