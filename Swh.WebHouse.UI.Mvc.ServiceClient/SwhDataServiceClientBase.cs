﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Swh.WebHouse.UI.Mvc.ServiceClient
{
    public abstract class SwhDataServiceClientBase
    {
        private readonly ISwhDataServiceClientConfig _swhDataServiceClientConfig;

        protected SwhDataServiceClientBase(ISwhDataServiceClientConfig swhDataServiceClientConfig)
        {
            _swhDataServiceClientConfig = swhDataServiceClientConfig;
        }

        private IHttpResponseHandler httpResponseHandler;

        protected abstract IHttpResponseHandler DefaultHttpResponseHandler { get; }

        public IHttpResponseHandler HttpResponseHandler
        {
            get
            {
                if (httpResponseHandler == null)
                    httpResponseHandler = DefaultHttpResponseHandler;

                return httpResponseHandler;
            }

            set
            {
                if (httpResponseHandler != null)
                    throw new NotSupportedException();

                httpResponseHandler = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected async Task<HttpClient> CreateHttpClientAsync(CancellationToken cancellationToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Culture", _swhDataServiceClientConfig.Culture);
            client.DefaultRequestHeaders.Add("AppId", _swhDataServiceClientConfig.ApplicationId);
            if (_swhDataServiceClientConfig.IsAuthCall)
            {
                client.DefaultRequestHeaders.Add("AuthCall", _swhDataServiceClientConfig.AuthMode);
            }
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Authorization", _swhDataServiceClientConfig.AuthToken);
            client.DefaultRequestHeaders.Add("TransactionUniqueIdentifier", _swhDataServiceClientConfig.TranId.ToString());
            client.DefaultRequestHeaders.Add("DtOffsetInMins", _swhDataServiceClientConfig.OffsetMinutes.ToString());
            return client;
        }

        protected async Task<HttpRequestMessage> CreateHttpRequestMessageAsync(CancellationToken cancellationToken)
        {
            return new HttpRequestMessage();
        }

        /// <summary>
        /// Processing response based on status.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <exception cref="HttpErrorOccuredException">Thrown if the <see cref="HttpResponseMessage.StatusCode"/> is not acceptable.</exception>
        protected void ProcessResponse(HttpClient request, HttpResponseMessage response)
        {
            HttpResponseHandler.HandleResponse(request, response);
        }       
    }
}
