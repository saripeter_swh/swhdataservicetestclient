﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Swh.DataService.Contract.Commands.Master.UserRights.Synchronization;
using Swh.DataService.Contract.Interfaces.Classes;
using Swh.WebHouse.UI.Mvc.ServiceClient;

namespace SwhDataServiceTestClient
{
    public partial class Form1 : Form
    {
        private string
            // Client application Key
            _clientKey = "uuiiwnnwe234nwewen",
            // Client application Id
            _applicationId = "tt232Fixsnp2d",
            // Ui culture
            _uiCulture = "hu-HU",
            _login = "secretsyncuser",
            _passWord = "Swhs3cr3t",
            _url = "http://195.70.51.35:8080";



        // Auth token
        private string _authToken;

        public Form1()
        {
            InitializeComponent();
        }

        private string GetAuthToken(bool loginCall)
        {
            if (loginCall)
            {
                return TripleDesEncrypt.Encrypt($"{tbLogin.Text}÷{tbPassword.Text}", tbClientKey.Text);
            }

            return TripleDesEncrypt.Encrypt(_authToken, tbClientKey.Text);
        }

        private ISwhDataServiceClientConfig GetConfig(bool loginCall)
        {
            return new SwhDataServiceClientConfig(tbAppId.Text, GetAuthToken(loginCall), _uiCulture, loginCall);
        }

        private PublicQueriesClient GetPublicQueriesClient(bool loginCall)
        {
            var client = new PublicQueriesClient(GetConfig(loginCall)) {BaseUrl = tbUrl.Text};
            return client;
        }

        private async void button5_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_authToken))
            {
                try
                {
                    var client = new CommandsClient(GetConfig(false)) {BaseUrl = tbUrl.Text};

                    await client.CreateUserWithContactSecretSyncAsync(
                        new CreateUserWithContactSecretSyncCommand
                        {
                            ContactEmail = "test",
                            ContactMobilePhone = "test",
                            OrganizationId = 1,
                            UserExternalIdentifier = 1,
                            UserLogin = "test",
                            UserName = "test"
                        });


                    MessageBox.Show("Sync call succeeded");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed.");
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please do a login call first");
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Authentication call!
                var client = GetPublicQueriesClient(true);

                var userResult = await client.UserLoginAsync();
                _authToken = userResult.UserAuthToken;

                if (!string.IsNullOrWhiteSpace(_authToken))
                {
                    MessageBox.Show("Succesful login! Username: " + userResult.User.Name);
                }
                else
                {
                    MessageBox.Show("Unsuccesful login!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }


        }

        private async void button4_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_authToken))
            {
                try
                {
                    var client = GetPublicQueriesClient(false);

                    var tasksOnTourResult = await client.TourPlannedTourAllocatedTasksListAsync(
                        new List<OrderInfoItem> {
                            new OrderInfoItem { OrderDirection = ListSortDirection.Ascending, OrderNo = 1, OrderbyField = "TaskID" } },
                       Guid.Parse("d75016c8-78d5-416c-a8c0-b35d3e2880cf"), 0, 20);


                    MessageBox.Show("Tasks on tour count: " + tasksOnTourResult.Items.Count());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed.");
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please do a login call first");
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_authToken))
            {
                try
                {
                    // Simple call, without auth token!
                    var client = GetPublicQueriesClient(false);
                    var plannedTourResult = await client.TourPlannedToursListAsync(
                        new List<OrderInfoItem> { new OrderInfoItem { OrderDirection = ListSortDirection.Ascending, OrderNo = 1, OrderbyField = "TourIdF" } }
                        , 0, 100, null,
                    new DateTimeOffset(2017, 10, 1, 0, 0, 0, TimeSpan.Zero),
                        new DateTimeOffset(2017, 10, 10, 0, 0, 0, TimeSpan.Zero), null);

                    MessageBox.Show("Planned tours count: " + plannedTourResult.Items.Count());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed.");
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please do a login call first");
            }
        }

        private async void button3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_authToken))
            {
                try
                {
                    var client = GetPublicQueriesClient(false);
                    var unallocatedTasksResult = await client.TransportTaskUnallocatedTasksListAsync(
                        new List<OrderInfoItem> {
                            new OrderInfoItem { OrderDirection = ListSortDirection.Ascending, OrderNo = 1, OrderbyField = "TransportOrderIDF" } },
                       new DateTimeOffset(2017, 10, 1, 0, 0, 0, TimeSpan.Zero),
                       new DateTimeOffset(2017, 10, 10, 0, 0, 0, TimeSpan.Zero),
                        0, 100, null, null, null, null,
                        new DateTimeOffset(2017, 10, 1, 0, 0, 0, TimeSpan.Zero),
                       new DateTimeOffset(2017, 10, 10, 0, 0, 0, TimeSpan.Zero));


                    MessageBox.Show("Unallocated tasks count: " + unallocatedTasksResult.Items.Count());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed.");
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                MessageBox.Show("Please do a login call first");
            }
        }
    }
}
